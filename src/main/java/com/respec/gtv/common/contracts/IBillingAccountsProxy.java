package com.respec.gtv.common.contracts;

import com.respec.gtv.accountservices.billingaccounts.models.Account;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


@SuppressWarnings({"PMD.UnnecessaryAnnotationValueElement", "PMD.UseObjectForClearerAPI"})
public interface IBillingAccountsProxy
{
    @RequestMapping(
            value = "/api/gtv/account-services/{tenant}/billing-accounts/list-accounts",
            method = RequestMethod.GET)
    ResponseEntity<List<Account>> listAccountsByTenantId(@PathVariable(value = "tenant") final String tenantId);

    @RequestMapping(
            value = "/api/gtv/account-services/{tenant}/billing-accounts/get-account/{accountid}",
            method = RequestMethod.GET)
    ResponseEntity<Account> getAccountById(
            @PathVariable(value = "tenant") final String tenantId,
            @PathVariable(value = "accountid") final int accountId);

}
