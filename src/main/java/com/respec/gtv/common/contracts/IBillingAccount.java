package com.respec.gtv.common.contracts;

import java.util.List;

public interface IBillingAccount<T>
{
   List<T> listAccountsByTenantId(String tenantId);
   T getAccountById(String tenantId, int accountId);
}
