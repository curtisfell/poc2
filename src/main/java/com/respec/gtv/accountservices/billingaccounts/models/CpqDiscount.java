
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class CpqDiscount implements Serializable
{

    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("deactivation_date")
    @Expose
    private String deactivationDate;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("recurrence")
    @Expose
    private String recurrence;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("application_type")
    @Expose
    private String applicationType;
    @SerializedName("price_type")
    @Expose
    private String priceType;
    @SerializedName("discount_key")
    @Expose
    private String discountKey;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -8418919077248676726L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CpqDiscount() {
    }

    /**
     * 
     * @param duration
     * @param recurrence
     * @param deactivationDate
     * @param applicationType
     * @param priceType
     * @param discountKey
     * @param id
     * @param value
     * @param startDate
     */
    public CpqDiscount(String startDate, String deactivationDate, String duration, String recurrence, String value, String applicationType, String priceType, String discountKey, String id) {
        super();
        this.startDate = startDate;
        this.deactivationDate = deactivationDate;
        this.duration = duration;
        this.recurrence = recurrence;
        this.value = value;
        this.applicationType = applicationType;
        this.priceType = priceType;
        this.discountKey = discountKey;
        this.id = id;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDeactivationDate() {
        return deactivationDate;
    }

    public void setDeactivationDate(String deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String recurrence) {
        this.recurrence = recurrence;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public String getDiscountKey() {
        return discountKey;
    }

    public void setDiscountKey(String discountKey) {
        this.discountKey = discountKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("startDate", startDate).append("deactivationDate", deactivationDate).append("duration", duration).append("recurrence", recurrence).append("value", value).append("applicationType", applicationType).append("priceType", priceType).append("discountKey", discountKey).append("id", id).toString();
    }

}
