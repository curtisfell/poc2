
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class DefaultBillingAccount implements Serializable
{

    @SerializedName("account_num")
    @Expose
    private String accountNum;
    @SerializedName("external_account_num")
    @Expose
    private String externalAccountNum;
    @SerializedName("cpq_discounts")
    @Expose
    private List<CpqDiscount> cpqDiscounts = null;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 7360178867827139794L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DefaultBillingAccount() {
    }

    /**
     * 
     * @param accountNum
     * @param externalAccountNum
     * @param cpqDiscounts
     * @param id
     */
    public DefaultBillingAccount(String accountNum, String externalAccountNum, List<CpqDiscount> cpqDiscounts, String id) {
        super();
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
        this.cpqDiscounts = cpqDiscounts;
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(String externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    public List<CpqDiscount> getCpqDiscounts() {
        return cpqDiscounts;
    }

    public void setCpqDiscounts(List<CpqDiscount> cpqDiscounts) {
        this.cpqDiscounts = cpqDiscounts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("accountNum", accountNum).append("externalAccountNum", externalAccountNum).append("cpqDiscounts", cpqDiscounts).append("id", id).toString();
    }

}
