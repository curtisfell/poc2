
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BillCycle implements Serializable
{

    @SerializedName("bill_cycle_type")
    @Expose
    private String billCycleType;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -7782130835578638623L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public BillCycle() {
    }

    /**
     * 
     * @param billCycleType
     * @param name
     * @param id
     */
    public BillCycle(String billCycleType, String name, String id) {
        super();
        this.billCycleType = billCycleType;
        this.name = name;
        this.id = id;
    }

    public String getBillCycleType() {
        return billCycleType;
    }

    public void setBillCycleType(String billCycleType) {
        this.billCycleType = billCycleType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("billCycleType", billCycleType).append("name", name).append("id", id).toString();
    }

}
