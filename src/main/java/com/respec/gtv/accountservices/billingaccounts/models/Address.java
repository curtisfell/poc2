
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Address implements Serializable
{

    @SerializedName("address_type")
    @Expose
    private String addressType;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 6824553408451028765L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Address() {
    }

    /**
     * 
     * @param addressType
     * @param id
     */
    public Address(String addressType, String id) {
        super();
        this.addressType = addressType;
        this.id = id;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressType", addressType).append("id", id).toString();
    }

}
