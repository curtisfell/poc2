package com.respec.gtv.accountservices.billingaccounts.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.respec.gtv.accountservices.billingaccounts.models.Account;
import com.respec.gtv.common.HttpResponseObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.respec.gtv.common.Request;
import com.respec.gtv.common.Response;
import org.apache.http.Header;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Service
public class GTVBillingAccountService
{

    private final GTVHttpClientService GTVHttpClientService;

    @Value("${gtv-config.port}")
    private String port;
    @Value("${gtv-config.connect-timeout}")
    private int connectTimeout;
    @Value("${spring.application.name}")
    private String appName;
    @Value("${gtv-config.api-key}")
    private String gtvApiKey;
    @Value("${gtv-config.api.base.url}")  // GTV URL
    private String billingAccountsUrl;
    @Value("${gtv-config.api.path.billing-accounts}")  // billing-accounts path
    private String billingAccountsPath;
    @Value("${gtv-config.api.path.billing-account-categories}")  // billing-accounts-categories
    private String billingAccountCategories;


    @Autowired
    public GTVBillingAccountService(final GTVHttpClientService GTVHttpClientService) {
        this.GTVHttpClientService = GTVHttpClientService;
    }


    public Response submitGet(final Request request) throws IOException, NoSuchAlgorithmException {

        String url = billingAccountsUrl + String.format(billingAccountsPath, request.getPageNumber(), request.getPageSize(), request.getAccountId());

        this.GTVHttpClientService.initializeClient(this.connectTimeout);

        final HttpGet httpGet = new HttpGet(url);
        httpGet.setHeaders(this.createRequestHeaders(gtvApiKey));
        final HttpResponseObject responseObject = this.GTVHttpClientService.execute(httpGet);

        if(!responseObject.isSuccessful())
        {
            // error
            return new Response();
        }

        //final Response response = this.deserializeResponse(responseObject.getResponseString());
        //final Response response = new Response(responseObject.isSuccessful(), responseObject.getResponseString());
        final Response response = new Response(responseObject.isSuccessful(), mapJsonToObject(responseObject));
        return response;

    }


    public Response submitPost(final Request request) throws IOException, NoSuchAlgorithmException {


        String url = String.format(billingAccountsPath, request.getPageNumber(), request.getPageSize(), request.getAccountId());

        this.GTVHttpClientService.initializeClient(this.connectTimeout);
        final String requestJson = this.serializeRequest(request);

        final HttpPost httpPost = new HttpPost(url);
        final HttpEntity requestEntity = new StringEntity(requestJson);
        httpPost.setEntity(requestEntity);

        httpPost.setHeaders(this.createRequestHeaders(gtvApiKey));
        final HttpResponseObject responseObject = this.GTVHttpClientService.execute(httpPost);

        if(!responseObject.isSuccessful())
        {
            // error
            return new Response();
        }

        //final Response response = this.deserializeResponse(responseObject.getResponseString());
        //final Response response = new Response(responseObject.isSuccessful(), responseObject.getResponseString());
        final Response response = new Response(responseObject.isSuccessful(), mapJsonToObject(responseObject));
        return response;
    }

    private Header[] createRequestHeaders(final String token) {

        return new Header[] {
                new BasicHeader("Accept", MediaType.APPLICATION_JSON_VALUE),
                new BasicHeader("Content-type", MediaType.APPLICATION_JSON_VALUE),
                new BasicHeader("X-Api-Key", token),
        };
    }

    private String serializeRequest (final Request request) throws JsonProcessingException {

        final ObjectMapper requestMapper = new ObjectMapper();

        return requestMapper.writeValueAsString(request);
    }

    private Response deserializeResponse (final String responseString) throws JsonProcessingException {

        final ObjectMapper responseMapper = new ObjectMapper();

        return responseMapper.readValue(responseString, Response.class);
    }

    private List<Account> mapJsonToObject(HttpResponseObject responseObject) {

        Gson gson = new Gson();
        Type accountListType = new TypeToken<List<Account>>(){}.getType();
        List<Account> list = gson.fromJson(responseObject.getResponseString(), accountListType);

        /*
        Account account = gson.fromJson(responseObject.getResponseString(), Account.class);
        List<Account> list = new ArrayList<>();
        list.add(account);
         */
        return list;

    }
}
