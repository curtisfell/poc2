package com.respec.gtv.accountservices.billingaccounts.controllers;

import com.respec.gtv.accountservices.billingaccounts.models.Account;
import com.respec.gtv.accountservices.billingaccounts.services.GTVBillingAccountService;
import com.respec.gtv.common.Request;
import com.respec.gtv.common.Response;
import com.respec.gtv.common.contracts.IBillingAccountsProxy;
import com.respec.gtv.accountservices.billingaccounts.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BillingAccountsController implements IBillingAccountsProxy
{
    private static final String DEFAULT_NOT_OK_STATUS_CODE = "UnhandledAccountServicesEx";

    @Value("${spring.application.name}")
    private String appName;

    @Value("${server.port}")
    private String port;

    private final EurekaInstanceConfigBean eurekaInstanceConfigBean;
    private final GTVBillingAccountService gtvBillingAccountService;

    @Autowired
    public BillingAccountsController(
            final EurekaInstanceConfigBean eurekaInstanceConfigBean,
            GTVBillingAccountService gtvBillingAccountService)
    {
        //Guard.againstNull(eurekaInstanceConfigBean, "eurekaInstanceConfigBean");
        this.eurekaInstanceConfigBean = eurekaInstanceConfigBean;
        this.gtvBillingAccountService = gtvBillingAccountService;
    }

    @RequestMapping(
            value = "/api/gtv/account-services/{tenant}/billing-accounts/list-accounts",
            method = RequestMethod.GET)
    public ResponseEntity<List<Account>> listAccountsByTenantId(
            @PathVariable(value = "tenant") final String tenantId)
    {
        final long start = System.currentTimeMillis();

        try
        {
            //AccountRepository repository = new AccountRepository();
            //List<Account> accounts = repository.listAccountsByTenantId(tenantId);

            /*Request request = new Request();
            request.setTenantId(tenantId);
            Response response = gtvBillingAccountService.submitGet(request);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Responded", "BillingAccountsController");
            return ResponseEntity.accepted().headers(headers).body(response.getAccounts());*/

            throw new Exception("endpoint not implemented");



        }
        catch (final Exception ex)
        {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Responded", "BillingAccountsController");
            return ResponseEntity.badRequest().headers(headers).body(null);
        }

    }


    @RequestMapping(
            value = "/api/gtv/account-services/{tenant}/billing-accounts/get-account/{accountid}",
            method = RequestMethod.GET)
    public ResponseEntity<Account> getAccountById(
            @PathVariable(value = "tenant") final String tenantId,
            @PathVariable(value = "accountid") final int accountId)
    {
        final long start = System.currentTimeMillis();

        try
        {
            //AccountRepository repository = new AccountRepository();
            //Account account = repository.getAccountById(tenantId, accountId);

            Request request = new Request();
            request.setTenantId(tenantId);
            request.setAccountId(accountId);

            Response response = gtvBillingAccountService.submitGet(request);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Responded", "BillingAccountsController");

            return ResponseEntity.ok().headers(headers).body(response.getAccounts().get(0));

        }
        catch (final Exception ex)
        {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Responded", "BillingAccountsController");
            return ResponseEntity.badRequest().headers(headers).body(null);
        }

    }
}
