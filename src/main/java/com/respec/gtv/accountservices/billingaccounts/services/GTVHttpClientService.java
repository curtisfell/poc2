package com.respec.gtv.accountservices.billingaccounts.services;

import com.respec.gtv.common.HttpResponseObject;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@Service
public class GTVHttpClientService
{
    private SSLConnectionSocketFactory sslSocketFactory;
    private RequestConfig requestConfig;

    public GTVHttpClientService(){};

    public void initializeClient (final int connectTimeout) throws NoSuchAlgorithmException
    {

        // set TLS
        this.sslSocketFactory= new SSLConnectionSocketFactory(
                SSLContext.getDefault(),
                new String[] { "TLSv1.2" },
                null,
                SSLConnectionSocketFactory.getDefaultHostnameVerifier());

        // set timeout config
        this.requestConfig = RequestConfig.custom()
                .setConnectTimeout(connectTimeout)
                .build();
    }

    public HttpResponseObject execute (final HttpPost postRequest) throws IOException
    {
        final HttpResponseObject httpResponseObject = new HttpResponseObject();

        // get response
        try(final CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(this.requestConfig)
                .setSSLSocketFactory(this.sslSocketFactory)
                .build();
            final CloseableHttpResponse httpResponse = httpClient.execute(postRequest))
        {
            httpResponseObject.setSuccessful(httpResponse != null && httpResponse.getStatusLine().getStatusCode() == 200);
            httpResponseObject.setResponseString(httpResponse == null ? "" : EntityUtils.toString(httpResponse.getEntity()));
        }

        return httpResponseObject;
    }

    public HttpResponseObject execute (final HttpGet getRequest) throws IOException
    {
        final HttpResponseObject httpResponseObject = new HttpResponseObject();

        // get response
        try(final CloseableHttpClient httpClient = HttpClientBuilder
                .create()
                .setDefaultRequestConfig(this.requestConfig)
                .setSSLSocketFactory(this.sslSocketFactory)
                .build();
            final CloseableHttpResponse httpResponse = httpClient.execute(getRequest))
        {
            httpResponseObject.setSuccessful(httpResponse != null && httpResponse.getStatusLine().getStatusCode() == 200);
            httpResponseObject.setResponseString(httpResponse == null ? "" : EntityUtils.toString(httpResponse.getEntity()));
        }

        return httpResponseObject;
    }


}
