
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ResponsibleParty implements Serializable
{

    @SerializedName("party_type")
    @Expose
    private String partyType;
    @SerializedName("customer_num")
    @Expose
    private String customerNum;
    @SerializedName("external_customer_num")
    @Expose
    private String externalCustomerNum;
    @SerializedName("default_billing_account")
    @Expose
    private DefaultBillingAccount defaultBillingAccount;
    @SerializedName("tax_id_number")
    @Expose
    private String taxIdNumber;
    @SerializedName("addresses")
    @Expose
    private List<Address> addresses = null;
    @SerializedName("contact_category")
    @Expose
    private ContactCategory contactCategory;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -7828289222293193545L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ResponsibleParty() {
    }

    /**
     * 
     * @param contactCategory
     * @param addresses
     * @param defaultBillingAccount
     * @param taxIdNumber
     * @param customerNum
     * @param id
     * @param partyType
     * @param externalCustomerNum
     */
    public ResponsibleParty(String partyType, String customerNum, String externalCustomerNum, DefaultBillingAccount defaultBillingAccount, String taxIdNumber, List<Address> addresses, ContactCategory contactCategory, String id) {
        super();
        this.partyType = partyType;
        this.customerNum = customerNum;
        this.externalCustomerNum = externalCustomerNum;
        this.defaultBillingAccount = defaultBillingAccount;
        this.taxIdNumber = taxIdNumber;
        this.addresses = addresses;
        this.contactCategory = contactCategory;
        this.id = id;
    }

    public String getPartyType() {
        return partyType;
    }

    public void setPartyType(String partyType) {
        this.partyType = partyType;
    }

    public String getCustomerNum() {
        return customerNum;
    }

    public void setCustomerNum(String customerNum) {
        this.customerNum = customerNum;
    }

    public String getExternalCustomerNum() {
        return externalCustomerNum;
    }

    public void setExternalCustomerNum(String externalCustomerNum) {
        this.externalCustomerNum = externalCustomerNum;
    }

    public DefaultBillingAccount getDefaultBillingAccount() {
        return defaultBillingAccount;
    }

    public void setDefaultBillingAccount(DefaultBillingAccount defaultBillingAccount) {
        this.defaultBillingAccount = defaultBillingAccount;
    }

    public String getTaxIdNumber() {
        return taxIdNumber;
    }

    public void setTaxIdNumber(String taxIdNumber) {
        this.taxIdNumber = taxIdNumber;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public ContactCategory getContactCategory() {
        return contactCategory;
    }

    public void setContactCategory(ContactCategory contactCategory) {
        this.contactCategory = contactCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("partyType", partyType).append("customerNum", customerNum).append("externalCustomerNum", externalCustomerNum).append("defaultBillingAccount", defaultBillingAccount).append("taxIdNumber", taxIdNumber).append("addresses", addresses).append("contactCategory", contactCategory).append("id", id).toString();
    }

}
