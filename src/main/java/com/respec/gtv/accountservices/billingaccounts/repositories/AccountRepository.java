package com.respec.gtv.accountservices.billingaccounts.repositories;

import com.respec.gtv.accountservices.billingaccounts.models.Account;
import com.respec.gtv.common.contracts.IBillingAccount;

import java.util.ArrayList;
import java.util.List;

public class AccountRepository implements IBillingAccount<Account>
{

   @Override
   public List<Account> listAccountsByTenantId(String tenantId) {

       List<Account> accounts = new ArrayList<>();
       accounts.add(mockAccount());
       return accounts;

   }

    @Override
    public Account getAccountById(String tenantId, int accountId) {

        Account account = mockAccount();

        return account;

    }

   private Account mockAccount() {

       Account a = new Account();

       return a;
   }

}
