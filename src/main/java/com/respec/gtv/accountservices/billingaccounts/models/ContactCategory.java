
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ContactCategory implements Serializable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = -2722199623228363082L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ContactCategory() {
    }

    /**
     * 
     * @param name
     * @param id
     */
    public ContactCategory(String name, String id) {
        super();
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("id", id).toString();
    }

}
