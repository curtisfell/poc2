
package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Account implements Serializable
{

    @SerializedName("account_num")
    @Expose
    private String accountNum;
    @SerializedName("external_account_num")
    @Expose
    private String externalAccountNum;
    @SerializedName("bill_cycle")
    @Expose
    private BillCycle billCycle;
    @SerializedName("responsible_party")
    @Expose
    private ResponsibleParty responsibleParty;
    @SerializedName("billing_account_category")
    @Expose
    private BillingAccountCategory billingAccountCategory;
    @SerializedName("bill_type")
    @Expose
    private String billType;
    @SerializedName("auto_payment_authorized")
    @Expose
    private String autoPaymentAuthorized;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_reason")
    @Expose
    private StatusReason statusReason;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 8405304366443790667L;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Account() {
    }

    /**
     * 
     * @param statusReason
     * @param accountNum
     * @param autoPaymentAuthorized
     * @param externalAccountNum
     * @param responsibleParty
     * @param billType
     * @param billingAccountCategory
     * @param id
     * @param billCycle
     * @param status
     */
    public Account(String accountNum, String externalAccountNum, BillCycle billCycle, ResponsibleParty responsibleParty, BillingAccountCategory billingAccountCategory, String billType, String autoPaymentAuthorized, String status, StatusReason statusReason, String id) {
        super();
        this.accountNum = accountNum;
        this.externalAccountNum = externalAccountNum;
        this.billCycle = billCycle;
        this.responsibleParty = responsibleParty;
        this.billingAccountCategory = billingAccountCategory;
        this.billType = billType;
        this.autoPaymentAuthorized = autoPaymentAuthorized;
        this.status = status;
        this.statusReason = statusReason;
        this.id = id;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public String getExternalAccountNum() {
        return externalAccountNum;
    }

    public void setExternalAccountNum(String externalAccountNum) {
        this.externalAccountNum = externalAccountNum;
    }

    public BillCycle getBillCycle() {
        return billCycle;
    }

    public void setBillCycle(BillCycle billCycle) {
        this.billCycle = billCycle;
    }

    public ResponsibleParty getResponsibleParty() {
        return responsibleParty;
    }

    public void setResponsibleParty(ResponsibleParty responsibleParty) {
        this.responsibleParty = responsibleParty;
    }

    public BillingAccountCategory getBillingAccountCategory() {
        return billingAccountCategory;
    }

    public void setBillingAccountCategory(BillingAccountCategory billingAccountCategory) {
        this.billingAccountCategory = billingAccountCategory;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getAutoPaymentAuthorized() {
        return autoPaymentAuthorized;
    }

    public void setAutoPaymentAuthorized(String autoPaymentAuthorized) {
        this.autoPaymentAuthorized = autoPaymentAuthorized;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public StatusReason getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(StatusReason statusReason) {
        this.statusReason = statusReason;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("accountNum", accountNum).append("externalAccountNum", externalAccountNum).append("billCycle", billCycle).append("responsibleParty", responsibleParty).append("billingAccountCategory", billingAccountCategory).append("billType", billType).append("autoPaymentAuthorized", autoPaymentAuthorized).append("status", status).append("statusReason", statusReason).append("id", id).toString();
    }

}
