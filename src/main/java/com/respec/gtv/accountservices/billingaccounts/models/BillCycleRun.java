package com.respec.gtv.accountservices.billingaccounts.models;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class BillCycleRun implements Serializable {

    @SerializedName("bill_run_type")
    @Expose
    private String billRunType;
    @SerializedName("step_status")
    @Expose
    private String stepStatus;
    @SerializedName("sequence")
    @Expose
    private String sequence;
    @SerializedName("step")
    @Expose
    private String step;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_date")
    @Expose
    private String statusDate;
    @SerializedName("open_date")
    @Expose
    private String openDate;
    @SerializedName("close_date")
    @Expose
    private String closeDate;
    @SerializedName("scheduled_run_date")
    @Expose
    private String scheduledRunDate;
    @SerializedName("invoice_due_date")
    @Expose
    private String invoiceDueDate;
    @SerializedName("invoice_date")
    @Expose
    private String invoiceDate;
    @SerializedName("processed_invoice_count")
    @Expose
    private String processedInvoiceCount;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;
    @SerializedName("use_payment_term")
    @Expose
    private String usePaymentTerm;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 4137107143662680250L;

    public String getBillRunType() {
        return billRunType;
    }

    public void setBillRunType(String billRunType) {
        this.billRunType = billRunType;
    }

    public String getStepStatus() {
        return stepStatus;
    }

    public void setStepStatus(String stepStatus) {
        this.stepStatus = stepStatus;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getOpenDate() {
        return openDate;
    }

    public void setOpenDate(String openDate) {
        this.openDate = openDate;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getScheduledRunDate() {
        return scheduledRunDate;
    }

    public void setScheduledRunDate(String scheduledRunDate) {
        this.scheduledRunDate = scheduledRunDate;
    }

    public String getInvoiceDueDate() {
        return invoiceDueDate;
    }

    public void setInvoiceDueDate(String invoiceDueDate) {
        this.invoiceDueDate = invoiceDueDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getProcessedInvoiceCount() {
        return processedInvoiceCount;
    }

    public void setProcessedInvoiceCount(String processedInvoiceCount) {
        this.processedInvoiceCount = processedInvoiceCount;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getUsePaymentTerm() {
        return usePaymentTerm;
    }

    public void setUsePaymentTerm(String usePaymentTerm) {
        this.usePaymentTerm = usePaymentTerm;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
