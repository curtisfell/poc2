package com.respec.gtv.accountservices.billingaccounts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationContextInitializedEvent;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.boot.context.event.ApplicationFailedEvent;
import org.springframework.boot.context.event.ApplicationPreparedEvent;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.event.ApplicationStartingEvent;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.EventListener;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;

@SpringBootApplication(scanBasePackages = {
        "com.respec.gtv.common.contracts",
        "com.respec.gtv.accountservices.billingaccounts.controllers",
        "com.respec.gtv.accountservices.billingaccounts.models",
        "com.respec.gtv.accountservices.billingaccounts.repositories",
        "com.respec.gtv.accountservices.billingaccounts.services",
        "com.respec.gtv.accountservices.billingaccounts.configurations"
})
//@EnableFeignClients(basePackages = {"com.respec.gtv.common.clientproxies"})
@EnableSwagger2
@Import(SpringDataRestConfiguration.class)
@EnableDiscoveryClient
public class BillingAccountsApplication
{
    public static void main(String[] args)
    {
        //LoggerFactory.getApacheLogger().setLevel(java.util.logging.Level.WARNING);

        SpringApplication.run(BillingAccountsApplication.class, args);
    }

    @EventListener(ApplicationStartingEvent.class)
    public void onApplicationStartingEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION STARTING: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationEnvironmentPreparedEvent.class)
    public void onApplicationEnvironmentPreparedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION ENVIRONMENT PREPARED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationContextInitializedEvent.class)
    public void onApplicationContextInitializedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION CONTEXT INITIALIZED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationPreparedEvent.class)
    public void onApplicationPreparedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION PREPARED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationStartedEvent.class)
    public void onApplicationStartedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION STARTED: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationReadyEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).info().message(String.format("[EVENT] APPLICATION READY: %s", this.getClass().getSimpleName())).log();
    }

    @EventListener(ApplicationFailedEvent.class)
    public void onApplicationFailedEvent()
    {
        //LoggerFactory.getStandardLogger(this.getClass()).error().message(String.format("[EVENT] APPLICATION FAILED TO START: %s", this.getClass().getSimpleName())).log();
    }
}
