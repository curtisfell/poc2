package com.respec.gtv.accountservices.billingaccounts.controllers;

import com.respec.gtv.accountservices.billingaccounts.BillingAccountsApplication;
import com.google.common.util.concurrent.UncheckedExecutionException;
//import org.junit.Before;
//import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = BillingAccountsApplication.class)
@AutoConfigureMockMvc
public class BillingAccountsControllerTest
{
    private String tenantId;
    private int accountId;

    @Autowired
    private MockMvc mockMvc;

    @Value("${gtv-config.port}")
    private String port;

    @BeforeAll
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);

        this.tenantId = "1";
        this.accountId = 1;
    }
/*
    @Test
    public void listBillingAccountsByTenantIdTest()
    {
        try
        {
            final MvcResult mvcResult = this.mockMvc.perform(
                    get("/api/gtv/account-services/{api-key}/{tenant}/billing-accounts/list-accounts", this.tenantId))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();

            assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
        }
        catch(final Exception ex)
        {
            throw new UncheckedExecutionException(ex);
        }

    }
*/
    @Test
    public void getBillingAccountByIdTest()
    {
        try
        {
            final MvcResult mvcResult = this.mockMvc.perform(
                    get("/api/gtv/account-services/{tenant}/billing-accounts/get-account/{accountid}", this.tenantId, this.accountId))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn();

            assertThat(mvcResult.getResponse().getStatus()).isEqualTo(HttpStatus.OK.value());
            //String x = mvcResult.getResponse().getContentAsString();

        }
        catch(final Exception ex)
        {
            throw new UncheckedExecutionException(ex);
        }

    }

}
