package com.respec.gtv.accountservices.billingaccounts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = BillingAccountsApplication.class)
public class BillingAccountsApplicationTest
{
    @Test
    public void contextLoads()
    {}

    /**
     * Tests the Spring Application context load.  This test will provide 100% line coverage for the static void main.
     */
    @Test
    public void test()
    {
        BillingAccountsApplication.main(new String[]{
                "--spring.main.web-environment=false",
                "--spring.autoconfigure.exclude=bogus"});
    }
}
