package com.respec.gtv.accountservices.billingaccounts;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@Configuration
public class EurekaFakeConfiguration
{
    @MockBean
    private EurekaInstanceConfigBean eurekaInstanceConfigBean;

    @Bean
    @Primary
    public EurekaInstanceConfigBean eurekaFargateConfig()
    {
        return this.eurekaInstanceConfigBean;
    }
}
